import os

from enum import Enum


class Flags(Enum):
    ID_ERROR = 'IdError'
    CONFIG_ERROR = 'ConfigError'
    FALSE_START_ERROR = 'FalseStartError'
    EMPTY_CELL_ERROR = 'EmptyCellError'
    UNMATCHED_VALUE_ERROR = 'UnmatcherValueError'


class FlagTracker:
    def __init__(self):
        self.flags = {'info': [], 'warn': [], 'error': [], 'verbose': []}
        self.configflags = {
            'provided': {'active': False, 'level': 'warn',
                         'message': 'No configuration provided. Proceeding'
                                    'with defaults...'},
            'datalocation': {'active': False, 'level': 'warn'},
            'idcolumns': {'active': False, 'level': 'error'},
            'idrequired': {'active': False, 'level': 'error'},
            'length': {'active': False, 'level': 'info',
                       'message': 'No lengths provided...'},
            'long': {'active': False, 'level': 'info',
                     'message': 'No maximum length provided...'},
            'short': {'active': False, 'level': 'info',
                      'message': 'No minimum length provided...'},
            'numeric': {'active': False, 'level': 'info',
                        'message': 'Not checking for numeric IDs...'},
            'termfile': {'active': False, 'level': 'error'},
            'regex': {'active': False, 'level': 'info',
                      'message': 'Not comparing IDs to regex...'},
            'nodata': {'active': False, 'level': 'error'}
        }

    def _getoutputstring(self, flaglistname):
        return self._strhelper(flaglistname)

    def _strhelper(self, level):
        outstr = 'Row\t\t\tError Type\t\t\t\tError Message\n'

        for configflag in self.configflags:
            if self.configflags[configflag]['active']:
                if self.configflags[configflag]['level'] == level:
                    errorstr = 'n/a' + '\t\t\t' + 'CONFIG_ERROR' + '\t\t' + \
                               str(self.configflags[configflag]['message']) + \
                               '\n'
                    outstr += errorstr

        lastrecord = 0
        for flag in self.flags[level]:
            for key in flag.keys():
                flagmessage = flag[key]['message']
                recordnumber = flag[key]['recordnumber']
                if lastrecord != recordnumber:
                    lastrecord = recordnumber
                    outstr += '\n'
                errorstr = '{0}\t\t\t{1}'.format(str(recordnumber),
                                                 str(key).split('.')[1])

                tabcount = (28 - len(errorstr)) / 4
                while tabcount > 0:
                    errorstr += '\t'
                    tabcount -= 1
                errorstr += flagmessage + '\n'
                outstr += errorstr
        return outstr

    def mergeflags(self, othertracker):
        for verbose in othertracker.flags['verbose']:
            self.flags['verbose'].append(verbose)

        for info in othertracker.flags['info']:
            self.flags['info'].append(info)

        for warn in othertracker.flags['warn']:
            self.flags['warn'].append(warn)

        for error in othertracker.flags['error']:
            self.flags['error'].append(error)

    def verbose(self, errortype, recordnumber, message):
        self.flags['verbose'] \
            .append({errortype: {'message': message,
                                 'recordnumber': recordnumber}})

    def info(self, infotype, recordnumber, message):
        self.flags['info'] \
            .append({infotype: {'message': message,
                                'recordnumber': recordnumber}})

    def warn(self, warningtype, recordnumber, message):
        self.flags['warn'] \
            .append({warningtype: {'message': message,
                                   'recordnumber': recordnumber}})

    def error(self, errortype, recordnumber, message):
        self.flags['error'] \
            .append({errortype: {'message': message,
                                 'recordnumber': recordnumber}})

    def config(self, configflag, *message):
        self.configflags[configflag]['active'] = True
        if len(message) > 0:
            self.configflags[configflag].update({'message': message[0]})

    def generateoutputfiles(self, flagdirectory):
        verbosefile = 'verbose-flags.txt'
        if flagdirectory:
            verbosefile = os.path.abspath(
                os.path.join(os.getcwd(), flagdirectory, verbosefile))
        with open(verbosefile, 'w') as f:
            f.write(self._getoutputstring('verbose'))

        infofile = 'info-flags.txt'
        if flagdirectory:
            infofile = os.path.abspath(
                os.path.join(os.getcwd(), flagdirectory, infofile))
        with open(infofile, 'w') as f:
            f.write(self._getoutputstring('info'))

        warningfile = 'warning-flags.txt'
        if flagdirectory:
            warningfile = os.path.abspath(
                os.path.join(os.getcwd(), flagdirectory, warningfile))
        with open(warningfile, 'w') as f:
            f.write(self._getoutputstring('warn'))

        errorfile = 'error-flags.txt'
        if flagdirectory:
            errorfile = os.path.abspath(
                os.path.join(os.getcwd(), flagdirectory, errorfile))
        with open(errorfile, 'w') as f:
            f.write(self._getoutputstring('error'))
