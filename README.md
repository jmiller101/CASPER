# CASPER

**Cleaning and Standardization Program for Educational Research**


### Prerequisites
---
1. Install Python
    - https://www.python.org/downloads/release
2. Download the program
    - https://gitlab.com/bluesroo/CASPER/
3. Understand the basics of JSON
    - https://www.tutorialspoint.com/json/json_syntax.htm

### Usage
---

####OS X and Linux:
1. Navigate to the downloaded file
2. Unzip the downloaded file
3. Edit the configuration (see next section)
5. Put your data files into the configured data location
4. Open Terminal and navigate to the unzipped folder
    - Press `Command` + `Space`
    - Type `terminal` and press `Enter`
    - Use `cd` to navigate to the downloaded folder
6. Type "python3 casper.py `configurationlocation`" (where 
`configurationlocation` is where you put your configuration file is
relative to the folder) into the console and press `Enter`

####Windows:
1. Navigate to the downloaded file
2. Unzip the downloaded file
3. Edit the configuration (see next section)
5. Put your data files into the configured data location
4. Open CMD and navigate inside of the unzipped folder
    - Press `Windows key` + `r`
    - Type `cmd` and press `Enter`
    - Use `cd` to navigate to the downloaded folder
6. Type "python3 casper.py `configurationlocation`" (where 
`configurationlocation` is where you put your configuration file is
relative to the folder) into the console and press `Enter`

### Configuration
---

**Defaults**:
```json
{
  "headeramount": 2,
  "falsestart": [
    1,
    2,
    3,
    5,
    6,
    7,
    8,
    9,
    10
  ],
  "emptycellvalue": -99,
  "conditionalcellvalue": -88,
  "conditionalcolumns": [
    {
      "seedcolumn": 283,
      "positivevalue": "2",
      "optionalcolumns": [
        284,
        285,
        286,
        287,
        288,
        289
      ]
    }
  ],
  "ids": {
    "idcolumns": [
      12,
      52,
      53,
      54,
      55
    ],
    "required": [
      11,
      52
    ],
    "length": {
      "min": 5,
      "max": 5
    },
    "numbersonly": true
  },
  "matchers": [
    {
      "name": "Nationality Matcher",
      "termfile": "matchers/nationalities.txt",
      "columns": [
        22
      ]
    }
  ],
  "datadirectory": "data"
}
```

**Possible Configurations**:

`datafile` (string): A relative path to the data file.
`datadirectory` (string): A relative path to the data directory.
**Note**: You must choose between using `datafile` or `datadirectory`.
`flagoutputdirectory` (string): A relative path to the output file for 
the flags.
`dataoutputdirectory` (string): A relative path to the output file for
the cleaned data.

`headeramount` (number): The number of headers in the data files.

`falsestart` (number): The minimum number of columns (starting from the
first one) that must be filled in order for the row to be considered a
real response.

`emptycellvalue` (string or number): The default value to fill empty
cells with.
`conditionalcellvalue` (string or number): The default value to fill
empty conditional cells with.
`conditionalcolumns` (array of objects): Not directly configurable.
Contains a list of all of the conditional columns. It is required if you
want to configure the conditional column checking.
`conditionalcolumns.seedcolumn` (number): The column which conditionally
decides whether the related columns are empty or not.
`conditionalcolumns.positivevalue` (string): The value which the seed
column must be in order for the optional columns to contain values.
`conditionalcolumns.optionalcolumns` (array of numbers): A list of
columns which should contain values if the seed column is the positive
value.

`ids`: (object): Not directly configurable. Contains other ID
configurations. It is required if you want to configure the ID checking.
`ids.idcolumns` (array of numbers): List of the columns that are IDs.
`ids.length` (number): Not directly configurable, but contains ID length
configurations.
`ids.length.min` (number): Minimum ID length.
`ids.length.max` (number): Maximum ID length.
`ids.numbersonly` (boolean): Can either be `true` or `false`. Denotes 
whether an ID must be numeric.

`matchers` (array): Not directly configurable. Contains all of the
matchers that need to be used.
`matchers[n].name` (string): The name of the matcher. Used for error
output.
`matchers[n].termfile` (string): A relative path to the file with all of
the terms that could be used for match.
`matchers[n].columns` (array of numbers): A list a the columns that need
to contain a value from ther termfile.

# CSV Combiner
Usage of the CSV combiner is virtually identical to the cleaner except
there is no need for the configuration file. On top of that, when you
run the cleaner it will automatically produce a raw combined csv file
for you named `combined.csv`.

The instructions to run this are similar to CASPER:

####OS X and Linux:
1. Navigate to the downloaded file
2. Unzip the downloaded file
3. Open Terminal and navigate to the unzipped folder
    - Press `Command` + `Space`
    - Type `terminal` and press `Enter`
    - Use `cd` to navigate to the downloaded folder
4. Type "python3 casper.py `configurationlocation`" (where 
`configurationlocation` is where you put your configuration file is
relative to the folder) into the console and press `Enter`

####Windows:
1. Navigate to the downloaded file
2. Unzip the downloaded file
3. Open CMD and navigate inside of the unzipped folder
    - Press `Windows key` + `r`
    - Type `cmd` and press `Enter`
    - Use `cd` to navigate to the downloaded folder
4. Type "python3 combiner.py `datalocation`" (where `datalocation` is where
you put your data relative to the folder) into the console and press
`Enter`

This utility will read all of the files in the folder that you specify
and combine them in a single file for easier viewing.
