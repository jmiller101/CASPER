import unittest
import os

from casper import Cleaner, CleanerConfig


class TestUtils:
    @staticmethod
    def getconfigpath(relativepath):
        return os.path.abspath(
            os.path.join(os.getcwd(), 'test-configs/' + relativepath))

    @staticmethod
    def getcleaner(configname):
        configpath = TestUtils.getconfigpath(configname)
        config = CleanerConfig(configpath)
        return Cleaner(config)


class BulkTests(unittest.TestCase):
    def testvalidids(self):
        configname = 'valid-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 0)
        self.assertEqual(len(cleaner.cleanrecords), 9)

    def testmismatchedids(self):
        configname = 'mismatched-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 9)
        self.assertEqual(len(cleaner.cleanrecords), 0)

    def testmissingids(self):
        configname = 'missing-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 9)
        self.assertEqual(len(cleaner.cleanrecords), 0)

    def testduplicateids(self):
        configname = 'duplicate-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 8)
        self.assertEqual(len(cleaner.cleanrecords), 1)


class IndividualTests(unittest.TestCase):
    def testshortids(self):
        configname = 'short-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 9)
        self.assertEqual(len(cleaner.cleanrecords), 0)

    def testlongids(self):
        configname = 'long-student-ids.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 6)
        self.assertEqual(len(cleaner.cleanrecords), 3)

    def testnonnumericids(self):
        configname = 'non-numeric-student-ids.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 5)
        self.assertEqual(len(cleaner.cleanrecords), 1)

    def testunmatchedregexids(self):
        configname = 'unmatched-regex-student-ids.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 5)
        self.assertEqual(len(cleaner.cleanrecords), 1)

    def testfalsestart(self):
        configname = 'false-start.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.cleanrecords), 3)

    def testfillemptycells(self):
        configname = 'empty-cells.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 7)
        self.assertEqual(len(cleaner.cleanrecords), 0)

    def testcheckconditionalcolumns(self):
        configname = 'conditional-logic.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 3)
        self.assertEqual(len(cleaner.cleanrecords), 1)

    def testmatchers(self):
        configname = 'column-matchers.config.json'
        cleaner = TestUtils.getcleaner(configname)

        self.assertEqual(len(cleaner.recordstocheck), 6)
        self.assertEqual(len(cleaner.cleanrecords), 1)
