import csv
import glob
import os
import sys


class Combiner:
    def __init__(self, datalocation, outputlocation):
        self.csvdata = []
        self.fieldnames = []

        if datalocation:
            self.datalocation = \
                os.path.abspath(os.path.join(os.getcwd(), datalocation))
        else:
            self.datalocation = \
                os.path.abspath(os.path.join(os.getcwd(), 'data'))

        if '.csv' in str(datalocation):
            self._singleload(self.datalocation)
        else:
            self._bulkload(self.datalocation)

        self._docombine(outputlocation)

    def _bulkload(self, dirpath):
        csvfiles = glob.glob(dirpath + '/*.csv')
        for filepath in csvfiles:
            self._singleload(filepath)

    def _singleload(self, filepath):
        with open(filepath, newline='') as csvfile:
            newheaderlist = self._fixheader(csvfile)
            reader = csv.DictReader(csvfile, fieldnames=newheaderlist)

            for fieldname in reader.fieldnames:
                if fieldname not in self.fieldnames:
                    self.fieldnames.append(fieldname)
            for row in reader:
                self.csvdata.append(row)

    def _docombine(self, outputdirectory):
        combinedfile = 'combined.csv'
        if outputdirectory:
            combinedfile = os.path.abspath(
                os.path.join(os.getcwd(), outputdirectory, combinedfile))
        with open(combinedfile, 'w', newline='',
                  encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, self.fieldnames)
            writer.writeheader()
            for row in self.csvdata:
                writer.writerow(row)

    def _fixheader(self, csvfile):
        header = csvfile.readline()
        columnnames = header.split(',')
        uniquenames = set()
        for i, column in enumerate(columnnames):
            columnnames[i] = column.strip()
            column = columnnames[i]
            if column in uniquenames:
                column = self._createnewcolumnname(column, uniquenames)
                columnnames[i] = column
            uniquenames.add(column)
        return columnnames

    @staticmethod
    def _createnewcolumnname(name, nameset):
        objectnumber = 1
        newname = str(objectnumber) + '_' + name
        while newname in nameset:
            objectnumber += 1
            newname = str(objectnumber) + '_' + name
        return newname


def main():
    if len(sys.argv) > 1:
        Combiner(sys.argv[1], sys.argv[2])
    else:
        Combiner(None, None)


if __name__ == "__main__":
    main()
