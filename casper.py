import csv
import json
import os
import re
import sys
import time

from combiner import Combiner
from flags import FlagTracker, Flags


class CleanerConfig:
    def __init__(self, configfilename):
        self.unset = False
        self._configcache = {}
        self.configfilename = configfilename

        if configfilename:
            with open(self.configfilename, newline='') as configfile:
                self._configjson = json.load(configfile)
        else:
            self.unset = True

    def _getconfigproperty(self, prop):
        if self.unset:
            return False

        if prop in self._configcache:
            return self._configcache[prop]

        if prop in self._configjson:
            configprop = self._configjson[prop]
            self._configcache[prop] = configprop
            return configprop
        else:
            return False

    def getdatafile(self):
        return self._getconfigproperty('datafile')

    def getdatadirectory(self):
        return self._getconfigproperty('datadirectory')

    def getflagoutputfile(self):
        return self._getconfigproperty('flagoutputdirectory')

    def getdataoutputfile(self):
        return self._getconfigproperty('dataoutputdirectory')

    def getidsconfig(self):
        return self._getconfigproperty('ids')

    def getfalsestart(self):
        return self._getconfigproperty('falsestart')

    def getemptycellvalue(self):
        return self._getconfigproperty('emptycellvalue')

    def getconditionalcellvalue(self):
        return self._getconfigproperty('conditionalcellvalue')

    def getconditionalcolumns(self):
        return self._getconfigproperty('conditionalcolumns')

    def getheaderamount(self):
        if self._getconfigproperty('headeramount'):
            return self._getconfigproperty('headeramount')
        else:
            return 1

    def getconditionalcellvalues(self):
        conditionalcellvalues = []

        if self.getconditionalcellvalue():
            conditionalcellvalues.append(self.getconditionalcellvalue())
            return conditionalcellvalues

        if self.getconditionalcolumns():
            for column in self.getconditionalcolumns():
                conditionalcellvalues.append(column['fillvalue'])
            return conditionalcellvalues
        else:
            return False

    def getmatchingcolumns(self):
        return self._getconfigproperty('matchers')


class CsvHelper:
    @staticmethod
    def checkheader(csvfile):
        hasheader = False

        if csv.Sniffer().has_header(csvfile.read(1024)):
            hasheader = True
        csvfile.seek(0)

        return hasheader


class Cleaner:
    def __init__(self, config):
        self.csvdata = []
        self.headers = []
        self.cleanrecords = []
        self.recordstocheck = []
        self.flagtracker = FlagTracker()

        self.config = config
        self.datalocation = os.path.abspath(os.path.join(os.getcwd(), 'data'))
        self.datalocationtype = 'directory'
        if config.getflagoutputfile():
            self.combineddatalocation = \
                os.path.abspath(os.path.join(os.getcwd(),
                                             config.getdataoutputfile(),
                                             'combined.csv'))
        else:
            self.combineddatalocation = \
                os.path.abspath(os.path.join(os.getcwd(), 'combined.csv'))

        self.matchers = dict()

        if self.config:
            self._setdatalocation(self.config)
        else:
            self.flagtracker.config('provided')

        print("Combining data files...")
        Combiner(self.datalocation, config.getdataoutputfile())
        print("Reading data into program for processing...")
        self._loaddata(self.combineddatalocation)

        if len(self.csvdata) > 0:
            print("Starting cleaning process on %s items..." %
                  len(self.csvdata))
            self._doclean()
        else:
            print("No data found...")
            self.flagtracker.config('nodata', 'No data was found at \'' +
                                    self.datalocation + '\'')

        print("Generating flag files...")
        self.flagtracker.generateoutputfiles(self.config.getflagoutputfile())
        print("Generating processes file...")
        self._generateoutputcsv(self.config.getdataoutputfile())

    def _setdatalocation(self, config):
        if config.getdatafile():
            self.datalocation = os.path.abspath(
                os.path.join(os.getcwd(), config.getdatafile()))
            self.datalocationtype = 'file'
        elif config.getdatadirectory():
            self.datalocation = os.path.abspath(
                os.path.join(os.getcwd(), config.getdatadirectory()))
            self.datalocationtype = 'directory'
        else:
            self.flagtracker.config('datalocation', 'No data location ' +
                                    'provided. Defaulting to \'' +
                                    self.datalocation + '\'')

    def _loaddata(self, filepath):
        with open(filepath, newline='') as csvfile:
            self.headers.append(csvfile.readline())
            position = csvfile.tell()
            for x in range(self.config.getheaderamount() - 1):
                self.headers.append(csvfile.readline())
            csvfile.seek(position)

            reader = csv.reader(csvfile)
            for row in reader:
                self.csvdata.append(row)

    def _doclean(self):
        self._truecurrentrecordnumber = 1
        self._removedrecords = 0

        checkedstudentids = set()

        for record in self.csvdata[:]:
            self._truecurrentrecordnumber += 1
            self._currentrecordnumber = \
                self._truecurrentrecordnumber - self._removedrecords - 1

            if self._currentrecordnumber < self.config.getheaderamount():
                continue
            self._readablerecordnumber = self._currentrecordnumber + 1

            isheader = False
            for header in self.headers:
                for field in str(header).split(','):
                    if record[0] == field:
                        isheader = True
                        break
                if isheader:
                    break
            if isheader:
                continue

            checkresults = []

            if self.config.getfalsestart():
                if not self._checkfalsestart(record):
                    self.removecurrentrecord()
                    self.flagtracker.error(Flags.FALSE_START_ERROR,
                                           self._truecurrentrecordnumber,
                                           'There was a false start on row ' +
                                           str(self._truecurrentrecordnumber) +
                                           ' of the combined CSV file (not ' +
                                           'the processed one)')
                    continue

            if self.config.getidsconfig():
                try:
                    checkresults.append(
                        self._checkstudentids(record, checkedstudentids))
                except ValueError:
                    self.removecurrentrecord()
                    continue

            if self.config.getemptycellvalue():
                checkresults.append(self._fillemptycells(record))

            if self.config.getconditionalcolumns():
                checkresults.append(self._checkconditionalcolumns(record))

            if self.config.getmatchingcolumns():
                checkresults.append(self._matchterms(record))

            if False in checkresults:
                self.recordstocheck.append(self._currentrecordnumber)
            else:
                self.cleanrecords.append(self._currentrecordnumber)

    def _fillemptycells(self, record):
        currentcolumn = 0
        containsempty = False
        emptycellvalue = self.config.getemptycellvalue()

        for column in record:
            if self.isemptycell(column):
                record[currentcolumn] = emptycellvalue
                containsempty = True
                self.flagtracker.verbose(Flags.EMPTY_CELL_ERROR,
                                         self._currentrecordnumber,
                                         'An empty cell was found: ' +
                                         str(currentcolumn))
            currentcolumn += 1

        return not containsempty

    def _checkconditionalcolumns(self, record):
        conditionsmet = True
        conditionalcellvalue = self.config.getconditionalcellvalue()
        conditionalcolumns = self.config.getconditionalcolumns()

        for column in conditionalcolumns:
            # If the seed column is empty then skip filling with -88
            if self.isemptycell(record[column['seedcolumn'] - 1]):
                continue
            elif record[column['seedcolumn'] - 1] == column['positivevalue']:
                for optional in column['optionalcolumns']:
                    if self.isemptycell(record[optional - 1]):
                        self.flagtracker.error(Flags.EMPTY_CELL_ERROR,
                                               self._readablerecordnumber,
                                               'A conditional empty cell '
                                               'should have been filled: ' +
                                               str(optional))
                        conditionsmet = False
            else:
                for optional in column['optionalcolumns']:
                    if not self.isemptycell(record[optional - 1]):
                        self.flagtracker.error(Flags.EMPTY_CELL_ERROR,
                                               self._readablerecordnumber,
                                               'A conditional empty cell '
                                               'should have been empty: ' +
                                               str(optional))
                        conditionsmet = False
                    else:
                        record[optional - 1] = conditionalcellvalue

        return conditionsmet

    def _checkstudentids(self, record, checkedids):
        studentidresults = StudentIdChecker(self.config.getidsconfig(),
                                            record,
                                            self._readablerecordnumber,
                                            checkedids)

        for studentid in studentidresults.newids:
            checkedids.add(studentid)

        self.flagtracker.mergeflags(studentidresults.dataflags)
        self.flagtracker.mergeflags(studentidresults.flagtracker)
        return studentidresults.isclean

    def _checkfalsestart(self, record):
        currentcolumn = 1
        for column in record:
            if self.isemptycell(column):
                if currentcolumn in self.config.getfalsestart():
                    return False
            currentcolumn += 1
        return True

    def _matchterms(self, record):
        matchesfound = True
        termlist = set()
        matchers = self.config.getmatchingcolumns()
        for matcher in matchers:
            matchername = matcher['name']
            matcherfile = os.path.abspath(os.path.join(os.getcwd(),
                                                       matcher['termfile']))

            try:
                with open(matcherfile, 'r', newline='') as termfile:
                    for line in termfile:
                        termlist.add(line.strip())
            except FileNotFoundError:
                self.flagtracker.config('termfile', 'The termfile for ' +
                                        matchername + ' was not found')
                matchesfound = False

            for column in matcher['columns']:
                valuetomatch = record[column - 1]
                if valuetomatch not in termlist:
                    self.flagtracker.error(Flags.UNMATCHED_VALUE_ERROR,
                                           self._readablerecordnumber,
                                           valuetomatch + ' did not match a ' +
                                           'value from ' + matchername)
                    matchesfound = False

        return matchesfound

    def isemptycell(self, cell):
        if not cell:
            return True
        if len(str(cell)) < 1:
            return True
        if cell == self.config.getemptycellvalue():
            return True
        if self.config.getconditionalcellvalues():
            if cell in self.config.getconditionalcellvalues():
                return True

        return False

    def removecurrentrecord(self):
        self.csvdata.pop(self._currentrecordnumber - 1)
        self._removedrecords += 1

    def _generateoutputcsv(self, outputdirectoryname):
        outputfilename = 'processed.csv'
        if outputdirectoryname:
            outputpath = os.path.abspath(
                os.path.join(os.getcwd(), outputdirectoryname, outputfilename))
            with open(outputpath, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(self.headers[0].strip().split(','))
                for row in self.csvdata:
                    writer.writerow(row)
        else:
            with open(outputfilename, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(self.headers[0].strip().split(','))
                for row in self.csvdata:
                    writer.writerow(row)


class StudentIdChecker:
    def __init__(self, config, record, readablerecordnumber, previousids):
        self.dataflags = FlagTracker()
        self.flagtracker = FlagTracker()
        self.isclean = True
        self.newids = set()

        # Create a list of the IDs in the record based on the idcolumns
        for column in config['idcolumns']:
            try:
                newid = record[column - 1]

                if not newid:
                    try:
                        if column in config['required']:
                            self.dataflags.error(Flags.ID_ERROR,
                                                 readablerecordnumber,
                                                 'A required ID was missing ' +
                                                 'at column ' + str(column))
                            self.isclean = False
                        else:
                            self.dataflags.info(Flags.ID_ERROR,
                                                readablerecordnumber,
                                                'A non-required ID was ' +
                                                'missing at column ' +
                                                str(column))
                        # If a null ID is found don't continue processing this
                        # row's IDs
                        continue
                    except KeyError:
                        self.dataflags.config('idrequired', 'Required ID ' +
                                              'columns were not specified')
                elif newid in previousids:
                    self.flagtracker.error(Flags.ID_ERROR,
                                           readablerecordnumber,
                                           'A duplicate ID was found at ' +
                                           'column ' + str(column) + '. ' +
                                           'Duplicate ID: ' + str(newid))
                    self.isclean = False

                self.newids.add(newid)
            except IndexError:
                self.flagtracker.config('idcolumns', 'An ID column number ' +
                                        'was out of range: ' + str(column))

        idamount = len(self.newids)
        self._checknoids(idamount)
        self._checkmismatchedids(idamount, readablerecordnumber)

        try:
            self._checkidlength(config['length'], self.newids,
                                readablerecordnumber)
        except KeyError:
            self.flagtracker.config('length')

        try:
            if config['numbersonly']:
                self._checknonnumeric(self.newids, readablerecordnumber)
        except KeyError:
            self.flagtracker.config('numeric')

        try:
            if config['regex']:
                self._checknonnumeric(self.newids, readablerecordnumber)
        except KeyError:
            self.flagtracker.config('regex')

    @staticmethod
    def _checknoids(idamount):
        # Check that there were any ids at all
        if idamount < 1:
            raise ValueError('There were no Student IDs')

    def _checkmismatchedids(self, idamount, readablerecordnumber):
        # Check that there was only 1 ID used in the record
        if idamount > 1:
            self.dataflags.error(Flags.ID_ERROR, readablerecordnumber,
                                 'There were mismatched IDs: ' +
                                 str(self.newids))
            self.isclean = False

    def _checkidlength(self, lengthconfig, idstocheck, readablerecordnumber):
        try:
            minlength = lengthconfig['min']
            for studentid in idstocheck:
                if len(str(studentid)) < minlength:
                    self.dataflags.error(Flags.ID_ERROR, readablerecordnumber,
                                         'An ID was too short: \'' +
                                         str(studentid) + '\'')
                    self.isclean = False
        except KeyError:
            self.flagtracker.config('short')

        try:
            maxlength = lengthconfig['max']

            for studentid in idstocheck:
                if len(str(studentid)) > maxlength:
                    self.dataflags.error(Flags.ID_ERROR, readablerecordnumber,
                                         'An ID  was too long: \'' +
                                         str(studentid) + '\'')
                    self.isclean = False
        except KeyError:
            self.flagtracker.config('long')

    def _checknonnumeric(self, idstocheck, readablerecordnumber):
        for studentid in idstocheck:
            if not str(studentid).isdigit():
                self.dataflags.error(Flags.ID_ERROR, readablerecordnumber,
                                     'An ID was not numeric: \'' +
                                     str(studentid) + '\'')
                self.isclean = False

    def _checkregex(self, regex, idstocheck, readablerecordnumber):
        matcher = re.compile(regex)

        for studentid in idstocheck:
            if not matcher.match(studentid):
                self.dataflags.error(Flags.ID_ERROR, readablerecordnumber,
                                     'An ID did not match the ' +
                                     'configutation\'s regex: \'' + studentid +
                                     '\'')
                self.isclean = False


def main():
    starttime = time.time()
    print("Reading configuration file...")
    if len(sys.argv) > 1:
        config = CleanerConfig(sys.argv[1])
    else:
        config = CleanerConfig(None)

    setupdirectories(config)
    Cleaner(config)
    print("Done in %s seconds" % round(time.time() - starttime, 2))


def setupdirectories(config):
    if config.getdataoutputfile():
        outputpath = os.path.abspath(
            os.path.join(os.getcwd(), config.getdataoutputfile()))
        if not os.path.exists(outputpath):
            os.makedirs(outputpath)

    if config.getflagoutputfile():
        flagpath = os.path.abspath(
            os.path.join(os.getcwd(), config.getflagoutputfile()))
        if not os.path.exists(flagpath):
            os.makedirs(flagpath)


if __name__ == "__main__":
    main()
